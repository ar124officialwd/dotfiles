#!/bin/zsh

export cwd=$(pwd)

GREEN='\033[0;32m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
NC='\033[0m'

# Set up zsh
echo && echo "${GREEN}<==> ${BLUE}Setting up --> zsh <--${NC}"
echo "${GREEN}<=> ${CYAN}Installing -> ohmyzsh <-${NC}"
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
ln -sf $(pwd)/zsh/dracula.zsh-theme ~/.oh-my-zsh/themes/
cd $cwd

echo "${GREEN}<=> ${CYAN}Copying -> .zshrc <- zsh configuration file${NC}"
cp ./.zshrc ~/
source ~/.zshrc
cd $cwd


# Set up tmux
echo "${GREEN}<==> ${BLUE}Setting up --> tmux <--${NC}"
echo "${GREEN}<=> ${CYAN}Installing -> tpm <- Tmux Package Manager${NC}"
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

echo "${GREEN}<=> ${CYAN}Copying -> .tmuxrc <- tmux configuration${NC}"
cp $(pwd)/.tmux.conf ~/


# Install common development utilities
echo && echo "${GREEN}<==> ${BLUE}Installing common --> development utils <--${NC}"
echo "${GREEN}<=> ${CYAN}Installing -> nvm <- Node Version Manager${NC}"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh)"

echo "${GREEN}<=> ${CYAN}Installing latest lts -> nodejs <-${NC}"
nvm install --lts

echo "${GREEN}<=> ${CYAN}Installing -> angular <- cli${NC}"
npm install -g yarn @angular/cli

echo "${GREEN}<=> ${CYAN}Installing -> laravel <- cli${NC}"
composer global require laravel/installer

echo "${GREEN}<=> ${CYAN}Installing -> deno <- cli${NC}"
curl -fsSL https://deno.land/x/install/install.sh | sh


# Set up neovim
echo && echo "${GREEN}<==> ${BLUE}Setup --> neovim <--${NC}"
echo "${GREEN}<=> ${CYAN}Creating configuration directory${NC}"
mkdir -p ~/.config/nvim

echo "${GREEN}<=> ${CYAN}Copying configuration${NC}"
cp -rvf $(pwd)/.config/nvim/. ~/.config/nvim/

echo "${GREEN}<=>${CYAN} Executing setup scripts${NC}"
source $(pwd)/vim-setup.sh
