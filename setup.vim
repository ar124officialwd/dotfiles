

echo 'Installing Plugins'
PlugInstall|q

echo 'Installing Coc Extensions'
CocInstall coc-angular coc-css coc-eslint coc-emmet coc-html coc-markdownlint coc-phpls coc-sql coc-tsserver coc-java | qall
