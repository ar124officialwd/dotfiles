call plug#begin()

" Use release branch (recommend)
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Or build from source code by using yarn: https://yarnpkg.com
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}

" NERDTREE
Plug 'scrooloose/nerdTree'

" Emeet
Plug 'mattn/emmet-vim'

" Surround
Plug 'tpope/vim-surround'

" Ctrlp
Plug 'ctrlpvim/ctrlp.vim'

" Airline 
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" dracula 
Plug 'dracula/vim', { 'as': 'dracula' }

call plug#end()



"Editor Configurations
set number
set tabstop=2
set shiftwidth=2
set expandtab
noremap <C-r> :source ~/.config/nvim/init.vim<CR>




"THEME SETTINGS
" set background=dark
colorscheme dracula
let g:airline_theme='simple'
if (has("termguicolors"))
	set termguicolors
endif



" coc-settings
let g:coc_global_extensions = ['coc-emmet', 'coc-css', 'coc-html', 'coc-json', 'coc-prettier', 'coc-tsserver']
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()




" nerd tree settings
nmap <C-n> :NERDTreeToggle<CR>




" emmet settings
let g:user_emmet_expandabbr_key = '<C-a>,'




""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" custom key mapping
" use alt+hjkl to move between split/vsplit panels
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>ltnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l


